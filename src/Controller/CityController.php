<?php

namespace App\Controller;

use App\Entity\City;
use App\Repository\CityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/city')]
class CityController extends AbstractController
{

    public function __construct(private CityRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(City $city)
    {

        return $this->json($city);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $city = $serializer->deserialize($request->getContent(), City::class, 'json');
            $this->repo->save($city, true);

            return $this->json($city, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

        
    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(City $city)
    {
        $this->repo->remove($city, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(City $city, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), City::class, 'json', [
                'object_to_populate' => $city
            ]);
            $this->repo->save($city, true);
            return $this->json($city);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/{id}/pictures", methods: 'GET')]
    public function getPictures(City $city)
    {

        return $this->json($city->getPictures());
    }
}